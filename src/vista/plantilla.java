package vista;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import javax.swing.*;

public class plantilla extends JFrame {

    public JButton principiante;
    public JButton intermedio;
    public JButton avanzado;
    public JLabel palabra;
    public JLabel imagen;
    public JButton A;
    public JButton B;
    public JButton C;
    public JButton D;
    public JButton E;
    public JButton F;
    public JButton G;
    public JButton H;
    public JButton I;
    public JButton J;
    public JButton K;
    public JButton L;
    public JButton M;
    public JButton N;
    public JButton Ñ;
    public JButton O;
    public JButton P;
    public JButton Q;
    public JButton R;
    public JButton S;
    public JButton T;
    public JButton U;
    public JButton V;
    public JButton W;
    public JButton X;
    public JButton Y;
    public JButton Z;

    int x = 33;
    int width = 734;
    private Propiedades pro = new Propiedades();

    public plantilla() throws IOException {
        super("ABCdario");

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        setSize(1000, 600);
        setMinimumSize(new Dimension(1000, 500));
        setPreferredSize(new Dimension(1000, 600));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());
        cp.add(fondo(), BorderLayout.CENTER);
        cp.add(fondoNivel(), BorderLayout.EAST);

        setVisible(true);
    }

    public JPanelFondo fondo() throws IOException {
        JPanelFondo fondo = new JPanelFondo();
        fondo.setLayout(new BorderLayout());
        URL url = getClass().getResource("/vista/imagenes/pizarra.png");
        fondo.setNuevo(url);
        fondo.add(titulo(), BorderLayout.NORTH);
        fondo.add(Juego(), BorderLayout.CENTER);
        fondo.add(Botones(), BorderLayout.SOUTH);
        return fondo;
    }

    public JPanelFondo fondoNivel() {
        JPanelFondo fondo = new JPanelFondo();
        fondo.setLayout(new BorderLayout());
        URL url = getClass().getResource("/vista/imagenes/pizarra.png");
        fondo.setNuevo(url);
        fondo.add(niveles(), BorderLayout.CENTER);
        return fondo;
    }

    public JPanel niveles() {
        JPanel ni = new JPanel();
        BoxLayout ly = new BoxLayout(ni, BoxLayout.Y_AXIS);
        ni.setLayout(ly);
        ni.add(Box.createGlue());
        ni.add(pro.formatoTexto("Selecciona un nivel  ", 16, Color.white));
        ni.add(Box.createRigidArea(new Dimension(50, 40)));
        ni.add(principiante = pro.formatoBoton("Principiante", 14, Color.white));
        ni.add(Box.createRigidArea(new Dimension(5, 5)));
        ni.add(intermedio = pro.formatoBoton("Intermedio", 14, Color.white));
        ni.add(Box.createRigidArea(new Dimension(5, 5)));
        ni.add(avanzado = pro.formatoBoton("Avanzado", 14, Color.white));
        ni.add(Box.createGlue());
        ni.setOpaque(false);
        return ni;
    }

    public JPanel titulo() {
        JPanel contenedor = new JPanel();
        BoxLayout ly = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(ly);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(pro.formatoTexto("ABCdario", 36, Color.WHITE));
        URL url = getClass().getResource("/vista/imagenes/Doki.png");
        panel.add(pro.ingresarImagen(url));
        panel.setOpaque(false);

        contenedor.add(pro.espacio(0, 50));
        contenedor.add(panel);
        contenedor.setOpaque(false);
        return contenedor;
    }

    public JPanel Juego() {
        JPanel contenedor = new JPanel();
        BoxLayout ly = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(ly);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        URL url = getClass().getResource("/vista/imagenes/Doki.png");
        panel.add(imagen = new JLabel(new ImageIcon(url)));
        panel.add(palabra = pro.formatoTexto("", 36, Color.WHITE));
        panel.setOpaque(false);

        contenedor.add(Box.createGlue());
        contenedor.add(panel);
        contenedor.add(Box.createGlue());
        contenedor.setOpaque(false);
        return contenedor;
    }

    public JPanel Botones() {
        JPanel contenedor = new JPanel();
        BoxLayout ly = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(ly);

        JPanel boton = new JPanel();
        GridLayout gl = new GridLayout(3, 8);
        gl.setHgap(5);
        gl.setVgap(5);
        boton.setLayout(gl);
        boton.add(A = pro.formatoBoton("A", 25, Color.WHITE));
        boton.add(B = pro.formatoBoton("B", 25, Color.WHITE));
        boton.add(C = pro.formatoBoton("C", 25, Color.WHITE));
        boton.add(D = pro.formatoBoton("D", 25, Color.WHITE));
        boton.add(E = pro.formatoBoton("E", 25, Color.WHITE));
        boton.add(F = pro.formatoBoton("F", 25, Color.WHITE));
        boton.add(G = pro.formatoBoton("G", 25, Color.WHITE));
        boton.add(H = pro.formatoBoton("H", 25, Color.WHITE));
        boton.add(I = pro.formatoBoton("I", 25, Color.WHITE));
        boton.add(J = pro.formatoBoton("J", 25, Color.WHITE));
        boton.add(K = pro.formatoBoton("K", 25, Color.WHITE));
        boton.add(L = pro.formatoBoton("L", 25, Color.WHITE));
        boton.add(M = pro.formatoBoton("M", 25, Color.WHITE));
        boton.add(N = pro.formatoBoton("N", 25, Color.WHITE));
        boton.add(Ñ = pro.formatoBoton("Ñ", 25, Color.WHITE));
        boton.add(O = pro.formatoBoton("O", 25, Color.WHITE));
        boton.add(P = pro.formatoBoton("P", 25, Color.WHITE));
        boton.add(Q = pro.formatoBoton("Q", 25, Color.WHITE));
        boton.add(R = pro.formatoBoton("R", 25, Color.WHITE));
        boton.add(S = pro.formatoBoton("S", 25, Color.WHITE));
        boton.add(T = pro.formatoBoton("T", 25, Color.WHITE));
        boton.add(U = pro.formatoBoton("U", 25, Color.WHITE));
        boton.add(V = pro.formatoBoton("V", 25, Color.WHITE));
        boton.add(W = pro.formatoBoton("W", 25, Color.WHITE));
        boton.add(X = pro.formatoBoton("X", 25, Color.WHITE));
        boton.add(Y = pro.formatoBoton("Y", 25, Color.WHITE));
        boton.add(Z = pro.formatoBoton("Z", 25, Color.WHITE));
        boton.setOpaque(false);

        contenedor.add(boton);
        contenedor.add(pro.espacio(0, 50));
        contenedor.setOpaque(false);
        return contenedor;
    }
}
