package vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Propiedades {

    public JLabel formatoTexto(String mensaje, int tamaño, Color color) {
        JLabel componente = new JLabel(mensaje);
        componente.setFont(new Font("Chalkduster", Font.BOLD, tamaño));
        componente.setForeground(color);
        return componente;
    }

    public JButton formatoBoton(String texto, int tamano_letra, Color color) {
        JButton componente = new JButton(texto);
        componente.setFont(new Font("Chalkduster", Font.BOLD, tamano_letra));
        componente.setForeground(color);
        componente.setOpaque(false);
        componente.setContentAreaFilled(false);
        componente.setBorderPainted(false);
        return componente;
    }

    public JLabel ingresarImagen(URL ruta) {
        JLabel icono = new JLabel(new ImageIcon(ruta));
        return icono;
    }
    public JLabel ingresarImagenS(String ruta) {
        JLabel icono = new JLabel(new ImageIcon(ruta));
        return icono;
    }

    public JPanel espacio(int ancho, int alto) {
        JPanel espacio = new JPanel();
        espacio.setPreferredSize(new Dimension(ancho, alto));
        espacio.setOpaque(false);
        return espacio;
    }
}
