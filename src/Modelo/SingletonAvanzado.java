package Modelo;

public class SingletonAvanzado {

    private static SingletonAvanzado instance;
    private static JuegoNormasFacade normas = new JuegoNormasFacade();
    private static JugadaMovimiento JM = new JugadaMovimiento();
    private static Iterator iter;
    private char[][] seleccion;

    private SingletonAvanzado() {
        seleccion = normas.preparativo("A");
        iter = JM.getIterator(seleccion[0], seleccion[1]);
        normas.imprimirPalabra(seleccion[0]);
    }

    public static SingletonAvanzado getInstance() {
        if (instance == null) {
            instance = new SingletonAvanzado();
        }
        return instance;
    }

    public boolean getJugada(char ingreso) {
        return iter.evaluar(ingreso);
    }

    public boolean getSiguiente() {
        return iter.hasNext();
    }

    public char[] getPalabra() {
        return seleccion[1];
    }

    public String getIcono() {
        String imagen = String.valueOf(seleccion[0]);
        return imagen;
    }
}
