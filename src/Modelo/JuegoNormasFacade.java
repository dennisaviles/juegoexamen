package Modelo;

public class JuegoNormasFacade implements normas {

    private static JuegoFactory juego = new PalabraFactory();

    @Override
    public void imprimirPalabra(char[] palabra) {
        juego.crearJuego().recorridoSeleccion(2, palabra);
    }

    @Override
    public void mostrarMensaje(String mensaje) {
        juego.crearJuego().mensajes(mensaje);
    }

    @Override
    public char[][] preparativo(String nivel) {
        String[] diccionario = juego.crearJuego().diccionariosJuego(nivel);
        String seleccion_aleatoria = juego.crearJuego().aleatorio(diccionario);
        char[] palabra_seleccionada = seleccion_aleatoria.toCharArray();
        char[] ocultar_palabra = juego.crearJuego().recorridoSeleccion(1, palabra_seleccionada).toCharArray();
        return new char[][]{palabra_seleccionada, ocultar_palabra};
    }

}
