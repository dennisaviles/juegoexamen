package Modelo;

public interface Juego {

    public String[] diccionariosJuego(String nivel);

    public String aleatorio(String[] diccionario);

    public String recorridoSeleccion(int corrido, char[] palabra);

    public void mensajes(String mensaje);
}
