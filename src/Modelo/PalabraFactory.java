package Modelo;

public class PalabraFactory extends JuegoFactory {

    @Override
    public Juego crearJuego() {
        return new Palabra();
    }

}
