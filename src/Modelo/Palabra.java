package Modelo;

public class Palabra implements Juego {

    private String[] principiante = {"CIRCULO", "CORAZON", "CUADRADO", "ECLIPSE", "ESTRELLA", "LUNA", "RECTANGULO", "TRAPECIO", "TRIANGULO"};

    private String[] intermedio = {"FACEBOOK", "INSTAGRAM", "PINTEREST", "TWITTER", "WHATSAPP", "GOOGLE", "MEGA", "NETFLIX", "YOUTUBE"};

    private String[] avanzado = {"CHEVROLET", "MERCEDES", "RENAULT", "SUBARU", "TOYOTA", "CITROEN", "MAZDA", "FERRARI", "SUZUKI"};

    @Override
    public String[] diccionariosJuego(String nivel) {
        String[] seleccionado = null;
        switch (nivel) {
            case "P":
                seleccionado = this.principiante;
                break;
            case "I":
                seleccionado = this.intermedio;
                break;
            case "A":
                seleccionado = this.avanzado;
                break;
        }
        return seleccionado;
    }

    @Override
    public String aleatorio(String[] diccionario) {
        int num = (int) (Math.random() * (diccionario.length));
        return diccionario[num];
    }

    @Override
    public String recorridoSeleccion(int corrido, char[] palabra) {
        if (corrido == 1) {
            String oculto = "";
            for (int i = 0; i <= palabra.length - 1; i++) {
                oculto = oculto + "_";
            }
            return oculto;
        } else {
            for (int i = 0; i <= palabra.length - 1; i++) {
                System.out.print(palabra[i] + " ");
            }
            System.out.print("\n\n");
            return null;
        }
    }

    @Override
    public void mensajes(String mensaje) {
        System.out.println(mensaje);
    }

}
