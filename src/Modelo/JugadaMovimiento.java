package Modelo;

public class JugadaMovimiento implements Movimiento {

    @Override
    public Iterator getIterator(char[] palabra_seleccionada, char[] palabra_secreta) {
        return new MovimientoIterator(palabra_seleccionada, palabra_secreta);
    }

    private class MovimientoIterator implements Iterator {

        private char[] palabra_seleccionada;
        private char[] palabra_secreta;
        private int movimientos;
        private int index;
        private char ingreso;

        private MovimientoIterator(char[] palabra_seleccionada, char[] palabra_secreta) {
            this.palabra_seleccionada = palabra_seleccionada;
            this.palabra_secreta = palabra_secreta;
            this.movimientos = palabra_secreta.length;
            this.index = 0;
        }

        @Override
        public boolean hasNext() {
            return this.index < this.movimientos;
        }

        @Override
        public Object next() {
            if (this.ingreso == this.palabra_seleccionada[this.index]) {
                this.palabra_secreta[this.index] = this.ingreso;
                this.index++;
                return true;
            }
            return false;
        }

        @Override
        public boolean evaluar(char ingreso) {
            this.ingreso = ingreso;
            return (boolean) this.next();
        }
    }

}
