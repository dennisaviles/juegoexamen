package Controlador;

import Modelo.SingletonAvanzado;
import Modelo.SingletonFacil;
import Modelo.SingletonIntermedio;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import vista.plantilla;

public class Controlador implements ActionListener {

    private plantilla vista;
    private SingletonFacil juegoP;
    private SingletonIntermedio juegoI;
    private SingletonAvanzado juegoA;
    private String seleccion = "";

    public Controlador() {
        try {
            vista = new plantilla();
            vista.A.addActionListener(this);
            vista.B.addActionListener(this);
            vista.C.addActionListener(this);
            vista.D.addActionListener(this);
            vista.E.addActionListener(this);
            vista.F.addActionListener(this);
            vista.G.addActionListener(this);
            vista.H.addActionListener(this);
            vista.I.addActionListener(this);
            vista.J.addActionListener(this);
            vista.K.addActionListener(this);
            vista.L.addActionListener(this);
            vista.M.addActionListener(this);
            vista.N.addActionListener(this);
            vista.Ñ.addActionListener(this);
            vista.O.addActionListener(this);
            vista.P.addActionListener(this);
            vista.Q.addActionListener(this);
            vista.R.addActionListener(this);
            vista.S.addActionListener(this);
            vista.T.addActionListener(this);
            vista.U.addActionListener(this);
            vista.V.addActionListener(this);
            vista.W.addActionListener(this);
            vista.X.addActionListener(this);
            vista.Y.addActionListener(this);
            vista.Z.addActionListener(this);
            vista.principiante.addActionListener(this);
            vista.intermedio.addActionListener(this);
            vista.avanzado.addActionListener(this);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.principiante) {
            juegoP = SingletonFacil.getInstance();
            this.seleccion = "P";
            URL url = getClass().getResource("/vista/diccionario/" + juegoP.getIcono() + ".png");
            vista.imagen.setIcon(new ImageIcon(url));
            vista.palabra.setText(String.valueOf(juegoP.getPalabra()));
        } else if (e.getSource() == vista.intermedio) {
            juegoI = SingletonIntermedio.getInstance();
            this.seleccion = "I";
            URL url = getClass().getResource("/vista/diccionario/" + juegoI.getIcono() + ".jpg");
            vista.imagen.setIcon(new ImageIcon(url));
            vista.palabra.setText(String.valueOf(juegoI.getPalabra()));
        } else if (e.getSource() == vista.avanzado) {
            juegoA = SingletonAvanzado.getInstance();
            this.seleccion = "A";
            URL url = getClass().getResource("/vista/diccionario/" + juegoA.getIcono() + ".jpg");
            vista.imagen.setIcon(new ImageIcon(url));
            vista.palabra.setText(String.valueOf(juegoA.getPalabra()));
        } else if (e.getSource() != vista.principiante && e.getSource() != vista.intermedio && e.getSource() != vista.avanzado) {
            ImageIcon icon;
            URL urlConfirmo = getClass().getResource("/vista/imagenes/confirmo.png");
            URL urlRechazo = getClass().getResource("/vista/imagenes/rechazo.png");
            if (this.seleccion != "") {
                JButton letra = (JButton) e.getSource();
                char rpt = letra.getText().charAt(0);
                switch (this.seleccion) {
                    case "P":
                        if (juegoP.getJugada(rpt)) {
                            icon = new ImageIcon(urlConfirmo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es CORRECTA", "Correcto", JOptionPane.DEFAULT_OPTION, icon);
                        } else {
                            icon = new ImageIcon(urlRechazo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es INCORRECTA", "Incorrecto", JOptionPane.DEFAULT_OPTION, icon);
                        }

                        vista.palabra.setText(String.valueOf(juegoP.getPalabra()));

                        if (!juegoP.getSiguiente()) {
                            if (JOptionPane.showConfirmDialog(null, "Gracias por jugar con nosotros. Desea una nueva partida?", "Adios", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                                vista.palabra.setText("");
                                vista.imagen.setIcon(null);
                                this.seleccion = "";
                            } else {
                                System.exit(0);
                            }
                        }
                        break;
                    case "I":
                        if (juegoI.getJugada(rpt)) {
                            icon = new ImageIcon(urlConfirmo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es CORRECTA", "Correcto", JOptionPane.DEFAULT_OPTION, icon);
                        } else {
                            icon = new ImageIcon(urlRechazo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es INCORRECTA", "Incorrecto", JOptionPane.DEFAULT_OPTION, icon);
                        }

                        vista.palabra.setText(String.valueOf(juegoI.getPalabra()));

                        if (!juegoI.getSiguiente()) {
                            if (JOptionPane.showConfirmDialog(null, "Gracias por jugar con nosotros. Desea una nueva partida?", "Adios", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                                vista.palabra.setText("");
                                vista.imagen.setIcon(null);
                                this.seleccion = "";
                            } else {
                                System.exit(0);
                            }
                        }
                        break;
                    case "A":
                        if (juegoA.getJugada(rpt)) {
                            icon = new ImageIcon(urlConfirmo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es CORRECTA", "Correcto", JOptionPane.DEFAULT_OPTION, icon);
                        } else {
                            icon = new ImageIcon(urlRechazo);
                            JOptionPane.showMessageDialog(null, "La letra ingresada es INCORRECTA", "Incorrecto", JOptionPane.DEFAULT_OPTION, icon);
                        }

                        vista.palabra.setText(String.valueOf(juegoA.getPalabra()));

                        if (!juegoA.getSiguiente()) {
                            if (JOptionPane.showConfirmDialog(null, "Gracias por jugar con nosotros. Desea una nueva partida?", "Adios", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                                vista.palabra.setText("");
                                vista.imagen.setIcon(null);
                                this.seleccion = "";
                            } else {
                                System.exit(0);
                            }
                        }
                        break;
                }

            } else {
                icon = new ImageIcon(urlRechazo);
                JOptionPane.showMessageDialog(null, "Debe seleccionar un nivel para empezar a jugar", "Error", JOptionPane.DEFAULT_OPTION, icon);
            }
        }
    }

}
