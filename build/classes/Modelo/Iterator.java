package Modelo;

public interface Iterator {

    public boolean hasNext();

    public Object next();

    public boolean evaluar(char ingreso);
}
